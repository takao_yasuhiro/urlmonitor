#AWS　安価な外形監視の構築（URL監視をする方法）

AWSはコストを最小構成であれば、月額数千円で
仮想サーバーが建てられるので、簡単な用途にも使えますね。
特に、オンプレでデータセンター構築したサーバー郡をインターネット経由で外形監視にも使えます。万一のオンプレのセンター障害を検知するのに使用できます。
外形監視に特化したシェルを作りました。

# 特徴
* URL監視でエラー時にメールを出す。
* 複数の宛先に同時に送る。
* モードを分けられる
 * 障害発生後も毎回　障害通知を出すモード
 * 障害発生した初回のみ送って、復旧後は復旧通知を出すモード

# 環境準備
## AWS EC2のインスタンスを作成

  t2.microを想定
 
## メールコマンドを設定。


```
sudo yum install mailx -y

```


実行結果

```
[ec2-user@ip-172-30-0-36 ~]$ sudo yum install mailx -y

途中略

インストール:
  mailx.x86_64 0:12.4-8.8.amzn1                                                 

完了しました!
```


## nkfのインストール

```
cd /tmp
wget http://mirror.centos.org/centos/6/os/x86_64/Packages/nkf-2.0.8b-6.2.el6.x86_64.rpm
sudo rpm -ivh nkf-2.0.8b-6.2.el6.x86_64.rpm
```

実行結果

```
[ec2-user@ip-172-30-0-36 ~]$ wget http://mirror.centos.org/centos/6/os/x86_64/Packages/nkf-2.0.8b-6.2.el6.x86_64.rpm

途中略
 

2016-04-29 12:59:07 (661 KB/s) - `nkf-2.0.8b-6.2.el6.x86_64.rpm' へ保存完了 [112856/112856]


[ec2-user@ip-172-30-0-36 ~]$ sudo rpm -ivh nkf-2.0.8b-6.2.el6.x86_64.rpm 

途中略
 
更新中 / インストール中...
   1:nkf-1:2.0.8b-6.2.el6             ################################# [100%]
```



## postfixのインストール、設定

```
sudo yum install postfix -y
sudo rm /etc/postfix/main.cf
```

/etc/postfix/main.cfを作成して編集

```
myhostname = hogehoge.jp
mydomain = hogehoge.jp
myorigin = hogehoge.jp
inet_interfaces = all
mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
home_mailbox = Maildir/
smtpd_banner = $myhostname ESMTP unknown
message_size_limit = 10485760
mynetworks = 10.0.0.0/16, 127.0.0.0/8
```

## sendmailを停止して、postfixを立ち上がるように

```
sudo service sendmail stop
chkconfig sendmail off
sudo chkconfig postfix on
sudo service postfix start
```

# 監視用のシェルの導入
作成しました公開中です。

[https://takao_yasuhiro@bitbucket.org/takao_yasuhiro/urlmonitor.git]
(https://takao_yasuhiro@bitbucket.org/takao_yasuhiro/urlmonitor.git)

## インストール
```
git clone https://takao_yasuhiro@bitbucket.org/takao_yasuhiro/urlmonitor.git
```

##設定ファイル情報


### 重要な項目を抜粋
```
#サービスID（英数字8文字以内）
SERVICE_ID=HOGEHOGE

#監視対象のURL
URL="https://www.facebook.com/“

#モニターモード MONITOR_MODE
#0:障害が継続している限りは毎回,障害通知を出す。
#  復旧した場合は、何も通知を出さない。
#1:障害が継続しても初回のみ障害通知を行う。
#  復旧した場合は、復旧の通知を出す。
MONITOR_MODE=0

#送信先のアドレスのリストファイル
ADDRESS_LIST_FILE=/home/ec2-user/urlmonitor/conf/mail.list

```

##送信先のアドレスのリストファイル
/home/ec2-user/urlmonitor/conf/mail.list

改行で複数のメールアドレスを記載する。

```
aaaaa@bbbb.com
aaaaa@bbbb.com

```


## 実行方法

```
/home/ec2-user/urlmonitor/bin/urlmonitor.sh 設定ファイル
/home/ec2-user/urlmonitor/bin/urlmonitor.sh /home/ec2-user/urlmonitor/conf/sample.conf 
```

障害通知メールサンプル

```
https://www.facebook.com/との通信でエラーが発生しました
 5分周期で監視しています
 障害が復旧するまで毎回通知します
 障害発生確認: 04/29 12:20:33
 再実行回数=3
 タイムアウト秒=8
 ----------------------------
 wgetの実行結果
 ----------------------------
 スパイダーモードが有効です。リモートファイルが存在してるか確認します。 --2016-04-29 12:20:33-- https://www.facebook.com/ve リモートファイルが存在していません -- リンクが壊れています!!!
```

復旧通知メールサンプル

```
https://www.facebook.com/ が復旧しました。
 障害発生確認: 04/29 11:06:11
 障害復旧確認: 04/29 11:10:25
```

## crontab に登録して5分周期で動くように調整




