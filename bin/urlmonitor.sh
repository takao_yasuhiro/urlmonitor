#!/bin/bash
#==============================================
# URL監視シェルスクリプト
# Takao Yasuhiro
#==============================================
if [ $# -eq 1 ]; then
  source $1
else
  echo "usage : $0 config_file "
  exit 1
fi
if [ -z "${SERVICE_ID+x}" ]; then
  echo "ERROR:config_fileにSERVICE_IDを定義してください."
  exit 1
fi
if [ -z "${URL+x}" ]; then
  echo "ERROR:config_fileにURLを定義してください."
  exit 1
fi
if [ -z "${TIMEOUT+x}" ]; then
  echo "ERROR:config_fileにTIMEOUTを定義してください."
  exit 1
fi
if [ -z "${TRYCOUNT+x}" ]; then
  echo "ERROR:config_fileにTRYCOUNTを定義してください."
  exit 1
fi
if [ -z "${MONITOR_MODE+x}" ]; then
  echo "ERROR:config_fileにMONITOR_MODEを定義してください."
  exit 1
fi
if [ -z "${MONITOR_PERIOD_MIN+x}" ]; then
  echo "ERROR:config_fileにMONITOR_PERIOD_MINを定義してください."
  exit 1
fi
if [ -z "${SUBJECT_FAILURE+x}" ]; then
  echo "ERROR:config_fileにSUBJECT_FAILUREを定義してください."
  exit 1
fi
if [ -z "${ADDRESS_LIST_FILE+x}" ]; then
  echo "ERROR:config_fileにADDRESS_LIST_FILEを定義してください."
  exit 1
fi
if [ ! -e $ADDRESS_LIST_FILE ]; then
  echo "ERROR:$ADDRESS_LIST_FILEが存在しません."
  exit 1
fi
if [ -z "${FROM_ADDRESS+x}" ]; then
  echo "ERROR:config_fileにを定義してください."
  exit 1
fi
if [ $MONITOR_MODE == 1 ] && [ -z "${STATUS_FILE+x}" ]; then
  echo "ERROR:MONITOR_MODE=1の場合は、STATUS_FILEを定義してください."
  exit 1
fi
if [ $MONITOR_MODE == 1 ] && [ -z "${SUBJECT_RECOVERY+x}" ]; then
  echo "ERROR:MONITOR_MODE=1の場合は、SUBJECT_RECOVERYを定義してください."
  exit 1
fi
sendMailx() {
    from="$1"
    to="$2"
    cc="$3"
    bcc="$4"
    subject="$5"
    contents="$6"
 
    inputEncoding="utf-8"
    subjectOutputEncoding="utf-8"
    outputEncoding="utf-8"
    subjectHead="=?$outputEncoding?B?"
    subjectBody="`echo "$subject" | iconv -f $inputEncoding -t $subjectOutputEncoding | base64 | tr -d '\n'`"
    subjectTail="?="
    fullSubject="$subjectHead$subjectBody$subjectTail"
    fullSubject="`echo "$subject" | nkf -M `"
    mailContents="`echo -e $contents | iconv -f $inputEncoding -t $outputEncoding`"
    echo "$mailContents" | mailx -s "$fullSubject" -c "$cc" -b "$bcc" -r "$from" "$to"
    return $?
}
wget_cmd="wget --no-check-certificate -S --spider -t $TRYCOUNT -T $TIMEOUT $URL "
alive=`$wget_cmd 2>&1 | grep -c "200 OK"`
timestamp=`date '+%m/%d %H:%M:%S'`
if [ -z "${STATUS_FILE+x}" ]; then
  STATUS_FILE="./"
fi
if [ $MONITOR_MODE == 0 ] || [ ! -e $STATUS_FILE ] ; then
  if [ $alive == 0 ] ; then
    #障害連絡
    wget_msg=`$wget_cmd 2>&1 | sed  "s/$/\\\n/g"`
    from=$FROM_ADDRESS
    subject="$SUBJECT_FAILURE $timestamp"
    if [ $MONITOR_MODE == 1 ]; then
      mode_msg="障害が継続しても初回のみ障害通知を行い、$SUBJECT_RECOVERY が来るまで障害が継続している事となります"
    else
      mode_msg="障害が復旧するまで毎回通知します"
    fi
    contents="$URLとの通信でエラーが発生しました\n
    $MONITOR_PERIOD_MIN分周期で監視しています\n
    $mode_msg\n
    障害発生確認: $timestamp\n
    再実行回数=$TRYCOUNT\n
    タイムアウト秒=$TIMEOUT\n
    ----------------------------\n
    wgetの実行結果\n
    ----------------------------\n
    $wget_msg
    "
    while read line
    do
      to=$line
      if [ $line != "" ] ; then
        sendMailx "$from" "$to" "$cc" "$bcc" "$subject" "$contents"
      fi
    done < $ADDRESS_LIST_FILE
    if [ $MONITOR_MODE == 1 ]; then
      echo $timestamp > $STATUS_FILE 
    fi
  fi
else
  if [ $MONITOR_MODE == 1  ] && [ -e $STATUS_FILE ] ; then
    if [ $alive == 1 ] ; then
      #復旧連絡
      failure_timestamp=`cat $STATUS_FILE `
      from=$FROM_ADDRESS
      subject="$SUBJECT_RECOVERY $timestamp"
      contents="$URL が復旧しました。\n
      障害発生確認: $failure_timestamp\n
      障害復旧確認: $timestamp\n
      "
      while read line
      do
        to=$line
        if [ $line != "" ] ; then
          sendMailx "$from" "$to" "$cc" "$bcc" "$subject" "$contents"
        fi
      done < $ADDRESS_LIST_FILE
      rm $STATUS_FILE 
    fi
  fi
fi

